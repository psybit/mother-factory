package net.psybit.pojo;

import java.io.File;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Project necessary info to create the corresponding pojo factories
 * 
 * @author n3k0
 *
 */
@Getter
@Setter
@ToString
public class FidderOptions {

	private File testSourceDirectory;
	private int stringsLength;
	private int integerLimit;
	private double floatPointFromLimit;
	private double floatPointToLimit;
	private int dataStructureSize;
	private boolean staticMethods;
	private boolean canIHazFidder;
	private String instanceName;
	private String createMethodName;

}