package net.psybit.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * For use in pojo generation code
 * 
 * @author n3k0
 *
 */
@Getter
@Setter
@ToString
public class SentenceWithParams {

	private String sentence;
	private boolean byConstructor;
	private List<Object> parameters = new ArrayList<>(0);

	public Object[] getParameters() {
		return parameters.toArray(new Object[0]);
	}

	public void addParameters(Object... objects) {
		for (Object object : objects) {
			this.parameters.add(object);
		}
	}

	public void setParameter(int index, Object object) {
		this.parameters.add(index, object);
	}

	public void addParameters(List<Object> objects) {
		this.parameters.addAll(objects);
	}
}