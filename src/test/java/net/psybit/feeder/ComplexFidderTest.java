package net.psybit.feeder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lombok.Getter;
import lombok.Setter;
import net.psybit.pojo.FidderOptions;
import net.psybit.pojo.MojoSettings;
import net.psybit.pojo.SentenceWithParams;

public class ComplexFidderTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ComplexFidderTest.class);

	private static final String NOMBRE_CAMPO = "Campo";

	private static MojoSettings mojoSettings;
	private static ComplexFidder COMPLEX_NO_EMBEDDED_SIMPLE_FIDDER;
	private static ComplexFidder COMPLEX_EMBEDDED_SIMPLE_FIDDER;
	private static List<String> CLASS_NAMES = Arrays.asList(CustomTwoElementsEnum.class.getCanonicalName(),
			CustomInterface.class.getCanonicalName(), CustomEmptyPojo.class.getCanonicalName(),
			CustomPojo.class.getCanonicalName());

	private StringBuilder stringBuilder;
	private List<Object> parameters;

	@BeforeTest
	public void before() {
		stringBuilder = new StringBuilder();
		parameters = new ArrayList<>();

		mojoSettings = new MojoSettings();
	}

	@BeforeTest(groups = { "embedded" })
	public void beforeEmbedded() {
		final FidderOptions fidderOptionsFidderTrue = new FidderOptions();
		fidderOptionsFidderTrue.setCanIHazFidder(true);
		fidderOptionsFidderTrue.setInstanceName("elNeim");
		fidderOptionsFidderTrue.setCreateMethodName("ejecutaLaMagnificaCreacionDe");

		mojoSettings.setFidderOptions(fidderOptionsFidderTrue);

		COMPLEX_EMBEDDED_SIMPLE_FIDDER = ComplexFidder.getInstance(fidderOptionsFidderTrue);
	}

	@BeforeTest(groups = { "no-embedded" })
	public void beforeNoEmbedded() {

		final FidderOptions fidderOptions = new FidderOptions();

		fidderOptions.setStringsLength(10);
		fidderOptions.setIntegerLimit(100);
		fidderOptions.setFloatPointFromLimit(10.1);
		fidderOptions.setFloatPointToLimit(100.1);
		fidderOptions.setDataStructureSize(2);

		mojoSettings.setFidderOptions(fidderOptions);

		COMPLEX_NO_EMBEDDED_SIMPLE_FIDDER = ComplexFidder.getInstance(fidderOptions);

	}

	@Test(groups = { "no-embedded" })
	public void testCustomEnum() {

		final SentenceWithParams sentenceWithParams = COMPLEX_NO_EMBEDDED_SIMPLE_FIDDER.createComplex(
				CustomTwoElementsEnum.class, NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		LOGGER.info("ComplexFidderTest.testCustomEnum >>> \n{} \n{}\n <<< OK! ", sentenceWithParams.getSentence(),
				Arrays.toString(sentenceWithParams.getParameters()));
	}

	@Test(groups = { "no-embedded" })
	public void testCustomInterface() {
		final SentenceWithParams sentenceWithParams = COMPLEX_NO_EMBEDDED_SIMPLE_FIDDER
				.createComplex(CustomInterface.class, NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		LOGGER.info("ComplexFidderTest.testCustomInterface >>> \n{} \n{}\n <<< OK! ", sentenceWithParams.getSentence(),
				Arrays.toString(sentenceWithParams.getParameters()));
	}

	@Test(groups = { "embedded" })
	public void testCustomEmptyPojo() {
		final SentenceWithParams sentenceWithParams = COMPLEX_EMBEDDED_SIMPLE_FIDDER
				.createComplex(CustomEmptyPojo.class, NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		LOGGER.info("\nComplexFidderTest.testCustomEmptyPojo >>> \n{} \n{}\n <<< OK!\n",
				sentenceWithParams.getSentence(), Arrays.toString(sentenceWithParams.getParameters()));
	}

	@Test(groups = { "embedded" })
	public void testCustomPojo() {
		final SentenceWithParams sentenceWithParams = COMPLEX_EMBEDDED_SIMPLE_FIDDER.createComplex(CustomPojo.class,
				NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		LOGGER.info("\nComplexFidderTest.testCustomPojo >>> \n{} \n{}\n <<< OK! \n", sentenceWithParams.getSentence(),
				Arrays.toString(sentenceWithParams.getParameters()));
	}

	// Fidder
	@Test(groups = { "embedded" })
	public void testCustomEnumFidder() {
		final SentenceWithParams sentenceWithParams = COMPLEX_EMBEDDED_SIMPLE_FIDDER.createComplex(
				CustomTwoElementsEnum.class, NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		LOGGER.info("\nComplexFidderTest.testCustomEnumFidder >>> \n{} \n{}\n <<< OK! \n",
				sentenceWithParams.getSentence(), Arrays.toString(sentenceWithParams.getParameters()));
	}

	@Test(groups = { "embedded" })
	public void testCustomInterfaceFidder() {
		final SentenceWithParams sentenceWithParams = COMPLEX_EMBEDDED_SIMPLE_FIDDER
				.createComplex(CustomInterface.class, NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		Assert.assertNotNull(stringBuilder);

		LOGGER.info("\nComplexFidderTest.testCustomInterfaceFidder >>> \n{} \n{}\n <<< OK! \n",
				sentenceWithParams.getSentence(), Arrays.toString(sentenceWithParams.getParameters()));
	}

	@Test(groups = { "embedded" })
	public void testCustomEmptyPojoFidder() {
		final SentenceWithParams sentenceWithParams = COMPLEX_EMBEDDED_SIMPLE_FIDDER
				.createComplex(CustomEmptyPojo.class, NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		LOGGER.info("\nComplexFidderTest.testCustomEmptyPojoFidder >>> \n{} \n{}\n <<< OK! \n",
				sentenceWithParams.getSentence(), Arrays.toString(sentenceWithParams.getParameters()));
	}

	@Test(groups = { "embedded" })
	public void testCustomPojoFidder() {
		final SentenceWithParams sentenceWithParams = COMPLEX_EMBEDDED_SIMPLE_FIDDER.createComplex(CustomPojo.class,
				NOMBRE_CAMPO, true, true, stringBuilder, parameters, CLASS_NAMES);

		LOGGER.info("\nComplexFidderTest.testCustomPojoFidder >>> \n{} \n{}\n <<< OK! \n",
				sentenceWithParams.getSentence(), Arrays.toString(sentenceWithParams.getParameters()));
	}

	// example objects to make test
	enum CustomOneElementEnum {
		CUSTOM_1
	}

	enum CustomTwoElementsEnum {
		CUSTOM_1, CUSTOM_2
	}

	enum CustomEmptyEnum {
	}

	interface CustomInterface {
	}

	class CustomEmptyPojo {
	}

	@Getter
	@Setter
	class CustomPojo {
		byte _byte;
		char _char;
		short _short;
		int _int;
		long _long;
		String string;
		Byte oByte;
		Character oChar;
		Short oShort;
		Integer oInt;
		Long oLong;
		String[] arrayOneDimension;
		Integer[][] arrayTwoDimensions;
		List<Long> list;
		List<CustomPojo> customList;
		Map<String, String> map;
		Map<CustomPojo, CustomPojo> customMap;
		Set<Character> set;
		Set<CustomPojo> customSet;
	}

	/*
	 *
	 *
	 * private static final Pattern PATTERN_INTEGER =
	 * Pattern.compile("pojo\\.setCampo\\(([0-9]{1,100})\\)\\;\\n?");
	 *
	 * private static final Pattern PATTERN_INTEGER_FIDDER = Pattern .compile(
	 * "pojo\\.setCampo\\(instance\\.create\\(java\\.lang\\.Integer\\.class\\)\\)\\;\\n?"
	 * );
	 *
	 * private static final Pattern PATTERN_DOUBLE = Pattern
	 * .compile("pojo\\.setCampo\\(([0-9]{1,5})(\\.)?([0-9]{0,20})?\\)\\;\\n?");
	 *
	 * private static final Pattern PATTERN_DOUBLE_FIDDER = Pattern
	 * .compile("pojo\\.setCampo\\(instance\\.create\\(double\\.class\\)\\)\\;\\n?")
	 * ;
	 *
	 * private static final Pattern PATTERN_ENUM = Pattern .compile(
	 * "pojo\\.setCampo\\(java\\.lang\\.Enum\\.values\\(\\)\\[0\\]\\)\\;\\n?");
	 *
	 * private static final Pattern PATTERN_ENUM_FIDDER = Pattern .compile(
	 * "pojo\\.setCampo\\(instance\\.create\\(java\\.lang\\.Enum\\.class\\)\\)\\;\\n?"
	 * );
	 *
	 * private static final Pattern PATTERN_CUSTOM_INTERFACE = Pattern.compile(
	 * "pojo\\.setCampo\\(\\(net\\.psybit\\.feeder\\.simple\\.FakeFidderTest\\.CustomInterface\\)null\\)\\;\\n?"
	 * );
	 *
	 * private static final Pattern PATTERN_CUSTOM_INTERFACE_FIDDER =
	 * Pattern.compile(
	 * "pojo\\.setCampo\\(instance\\.create\\(net\\.psybit\\.feeder\\.simple\\.FakeFidderTest\\.CustomInterface\\.class\\)\\)\\;\\n?"
	 * );
	 *
	 * private static final Pattern PATTERN_CUSTOM_EMPTY_ENUM_FIDDER =
	 * Pattern.compile(
	 * "pojo\\.setCampo\\(instance\\.create\\(net\\.psybit\\.feeder\\.simple\\.FakeFidderTest\\.CustomEmptyEnum\\.class\\)\\)\\;\\n?"
	 * );
	 *
	 * private static final Pattern PATTERN_CUSTOM_EMPTY_ENUM = Pattern.compile(
	 * "pojo\\.setCampo\\(net\\.psybit\\.feeder\\.simple\\.FakeFidderTest\\.CustomEmptyEnum\\.values\\(\\)\\[0\\]\\)\\;\\n?"
	 * );
	 *
	 *
	 *
	 * @Test public void testInstanceIntDoubleDouble() { ComplexFidder _100_INSTANCE
	 * = ComplexFidder.instance(100, 100, 100); Assert.assertNotNull(_100_INSTANCE);
	 *
	 * LOGGER.info("testInstanceIntDoubleDouble {}", _100_INSTANCE); }
	 *
	 * @Test public void testCreatePrimitive() { CodeBlock stringBlock =
	 * DEFAULT_INSTANCE.create(double.class, NOMBRE_CAMPO, false);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_DOUBLE.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreatePrimitive {}", stringBlock); }
	 *
	 * @Test public void testCreatePrimitiveCanIHazFidder() { CodeBlock stringBlock
	 * = DEFAULT_INSTANCE.create(double.class, NOMBRE_CAMPO, true);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_DOUBLE_FIDDER.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreatePrimitive {}", stringBlock); }
	 *
	 * @Test public void testCreateEnum() { CodeBlock stringBlock =
	 * DEFAULT_INSTANCE.create(Enum.class, NOMBRE_CAMPO, false);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_ENUM.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreateEnum {}", stringBlock); }
	 *
	 * @Test public void testCreateCustomEmptyEnum() { CodeBlock stringBlock =
	 * DEFAULT_INSTANCE.create(CustomEmptyEnum.class, NOMBRE_CAMPO, false);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_CUSTOM_EMPTY_ENUM.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreateEnum {}", stringBlock); }
	 *
	 * @Test public void testCreateCustomEmptyEnumCanIHazFidder() { CodeBlock
	 * stringBlock = DEFAULT_INSTANCE.create(CustomEmptyEnum.class, NOMBRE_CAMPO,
	 * true); Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_CUSTOM_EMPTY_ENUM_FIDDER.matcher(toMatch).matches()
	 * );
	 *
	 * LOGGER.info("testCreateEnum {}", stringBlock); }
	 *
	 * @Test public void testCreateEnumCanIHazFidder() { CodeBlock stringBlock =
	 * DEFAULT_INSTANCE.create(Enum.class, NOMBRE_CAMPO, true);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_ENUM_FIDDER.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreateEnum {}", stringBlock); }
	 *
	 * @Test public void testCreateInteger() { CodeBlock stringBlock =
	 * DEFAULT_INSTANCE.create(Integer.class, NOMBRE_CAMPO, false);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_INTEGER.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreateObject {}", stringBlock); }
	 *
	 * @Test public void testCreateObjectCanIHazFidder() { CodeBlock stringBlock =
	 * DEFAULT_INSTANCE.create(Integer.class, NOMBRE_CAMPO, true);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_INTEGER_FIDDER.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreateObject {}", stringBlock); }
	 *
	 * @Test public void testCreateCustomInterface() { CodeBlock stringBlock =
	 * DEFAULT_INSTANCE.create(CustomInterface.class, NOMBRE_CAMPO, false);
	 * Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_CUSTOM_INTERFACE.matcher(toMatch).matches());
	 *
	 * LOGGER.info("testCreateObject {}", stringBlock); }
	 *
	 * @Test public void testCreateCustomInterfaceCanIHazFidder() { CodeBlock
	 * stringBlock = DEFAULT_INSTANCE.create(CustomInterface.class, NOMBRE_CAMPO,
	 * true); Assert.assertNotNull(stringBlock);
	 *
	 * String toMatch = stringBlock.toString();
	 *
	 * Assert.assertTrue(PATTERN_CUSTOM_INTERFACE_FIDDER.matcher(toMatch).matches())
	 * ;
	 *
	 * LOGGER.info("testCreateCustomInterfaceCanIHazFidder {}", stringBlock); }
	 *
	 *
	 *
	 */

}