package net.psybit.poet;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.squareup.javapoet.MethodSpec;

import lombok.Data;
import net.psybit.pojo.FidderOptions;
import net.psybit.pojo.MojoSettings;
import net.psybit.utils.Constants;

public class FactoryCoderTest {

	private static FactoryCoder FACTORY_CODER;

	static FidderOptions fidderOptions;
	static MojoSettings mojoSettings;

	@BeforeClass
	public static void beforeClass() {
		fidderOptions = new FidderOptions();
		fidderOptions.setStringsLength(10);
		fidderOptions.setIntegerLimit(100);
		fidderOptions.setFloatPointFromLimit(10.1);
		fidderOptions.setFloatPointToLimit(100.1);
		fidderOptions.setDataStructureSize(2);
		fidderOptions.setCreateMethodName("crear");
		
		mojoSettings = new MojoSettings();
		mojoSettings.setFidderOptions(fidderOptions);

		FACTORY_CODER = new FactoryCoder(fidderOptions);
	}

	@Test
	public void buildNameCustomEmptyPojoTest() {
		String s = FACTORY_CODER.buildName(CustomEmptyPojo.class);

		Assert.assertNotNull(s);
	}

	@Test
	public void buildNameCustomPojoTest() {
		String s = FACTORY_CODER.buildName(CustomPojo.class);

		Assert.assertNotNull(s);
	}

	@Test
	public void createPojoCustomPojoTest() {
		Constants.projectClasses().add(CustomPojo.class);
		MethodSpec m = FACTORY_CODER.createPojo(CustomPojo.class);

		Assert.assertNotNull(m);
	}

	static class CustomEmptyPojo {

	}

	@Data
	static class CustomPojo {
		byte _byte;
		char _char;
		short _short;
		int _int;
		long _long;
		String string;
		Byte oByte;
		Character oChar;
		Short oShort;
		Integer oInt;
		Long oLong;
		String[] arrayOneDimension;
		Integer[][][][] arrayTwoDimensions = {{{{0}}}};
		List<Long> list;
		List<CustomPojo> customList;
		Map<String, String> map;
		Map<CustomPojo, CustomPojo> customMap;
		Map<String, String[]> mapArray;
		Set<Character> set;
		Set<CustomPojo> customSet;
	}
}
